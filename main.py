import pandas as pd

df = pd.read_excel("dataset/train.xlsx")

df1 = pd.read_excel("dataset/test.xlsx")

acc = 0

rec = 0

for index1, row1 in df1.iterrows():
    mostSim = 0
    maxSimPer = 0
    for index, row in df.iterrows():
        simPer = 0
        for key in row1.to_dict():
            if key == 'MJIL 1000 08 10':
                break
            if row1.to_dict()[key] == row.to_dict()[key]:
                simPer += 1
        if maxSimPer < simPer:
            maxSimPer = simPer
            mostSim = index
    flag = False
    accuracy = 0
    myRecoil = 0
    recoil = 0
    for key in row1.to_dict():
        if key == 'MJIL 1000 08 10' or flag:
            flag = True
            if row1.to_dict()[key] == 1:
                recoil += 1
            if df.iloc[mostSim].to_dict()[key] == row1.to_dict()[key]:
                accuracy += 1
                if df.iloc[mostSim].to_dict()[key] == 1:
                    myRecoil += 1

    acc += accuracy*100/111
    rec += myRecoil*100/recoil
    print("Accuracy = ", accuracy*100/111)
    print("Recoil = ", recoil - myRecoil)
print("#####################################")
print("Average accuracy = ", acc/42)
print("Average recoil = ", rec/42)
